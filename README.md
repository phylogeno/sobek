# Sobek - a parallel version of CroCo

Sobek is a template-based script. It allows parallelization at two levels: a BLAST parallelization with the `blast_threads` argument and RapMap parallelization with the `rapmap_theads` argument. Other arguments are mandatory as well, such as `src_dir` (directory containing all scripts and templates). All the remaining arguments serve the purpose of controlling the flow of the different tasks needed to complete the analysis: `idle_delay` (in seconds; halts the flow when the previous task needs to be completed before executing the next one), `job_execution_delay` (in seconds; delay ensuring tasks are properly executed and registered into the SGE scheduler ensuring proper functionality of conditional job execution) and `sge_jobid_digits` (number of digits composing the job identifiers; ensures that job identifiers are properly stored for conditional job execution). In addition, for a big index that requires a lot of RAM, the `index_split_factor` (N; split the index in N) argument allows to run Sobek on a split-index based heuristic.

## Installation

### CroCo

The first step is to install CroCo (<https://gitlab.mbb.univ-montp2.fr/mbb/CroCo#quick-installation-guide>).

### Dependencies

As Sobek relies on some custom Perl scripts, two Perl modules are needed. They can be installed using `cpanm`:

```
$ cpanm Bio::MUST::Core
$ cpanm Template
```

### Sobek

Then, you can install Sobek as follow:

```
$ git clone git@bitbucket.org:phylogeno/sobek.git
$ cd sobek
$ tpage --define croco_src_dir=/PATH/TO/CROCO/SRC/ install.tt > install.sh
$ cat install.sh | sh
```

## Usage

### Sobek - single index

```
$ tpage --define indir=$(pwd) --define src_dir=~/CroCo/Sobek/src \
    --define blast_threads=10 --define rapmap_threads=20 \
    --define idle_delay=10 --define job_execution_delay=60 \
    --define sge_jobid_digits=7 ~/CroCo/Sobek/src/templates/Sobek.tt > Sobek.sh

$ cat Sobek.sh | sh
```

### Sobek - split index

```
$ tpage --define indir=$(pwd) --define src_dir=~/CroCo/Sobek/src \
    --define index_split_factor=10 --define blast_threads=10 \
    --define rapmap_threads=20 --define idle_delay=10 --define job_execution_delay=60 \
    --define sge_jobid_digits=7 \
	~/CroCo/Sobek/src/templates/Sobek-splitindex.tt > Sobek-splitindex.sh

$ cat Sobek-splitindex.sh | sh
```

## References

- Simion et al. (2018) A software tool 'CroCo' detects pervasive cross-species contamination in next generation sequencing data. BMC Biol. 16(1):28.
- Van Vlierberghe et al. (2021) Decontamination, pooling and dereplication of the 678 samples of the Marine Microbial Eukaryote Transcriptome Sequencing Project (submitted to BMC Res Notes).


## License and Copyright

This software is copyright (c) 2021 by University of Liege / Unit of Eukaryotic Phylogenomics / Mick VAN VLIERBERGHE and Denis BAURAIN.

This is free software; you can redistribute it and/or modify it under the same terms as the Perl 5 programming language system itself.

