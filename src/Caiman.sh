my_dir="$(dirname "$0")"
source "$1"

### setting sample names
i=0
for fasta in $INDIR/*.fasta ; do
   fasta_array[$i]=`basename $fasta .fasta`
   i=$(( i + 1 ))
done

# extract fasta for suspects
if [ $2 == "blastdbcmd" ]; then
    source "$my_dir/blastdbcmd-cmd.sh"
    exit 1
fi

# clean and concatenate files
if [ $2 == "cleancat" ]; then
    source "$my_dir/blastdbcmd-clean.sh"
    echo "Done cleaning and 'cating' files..."
    exit 1
fi

### index contigs for the selected tool
toolidx="ALL_transcripts_rapmap_index"
if [ $2 == "index" ]; then
    if [ ! -d $out/$toolidx ]; then 
    echo "rapmap quasiindex -t $out/ALL_transcripts.fasta -p -x $3 -i $out/$toolidx" > $INDIR/jobs/rapmap-quasiindex.cmd; echo "Done preparing rapamap quasiindex command"; exit 1; fi
fi

if [ $2 == "quasimap" ]; then
    # getting length of transcripts
    for (( j=0; j <i; j++ ))
    do
      ref=${fasta_array[$j]};
      refseqs=$out/$ref".ctgs"
      echo -e "Getting length of $ref transcripts"
      awk -v ref=$ref -v refseqs=$refseqs 'BEGIN{RS=">"; FS="\t"}
      NR>1   {
       sub("\n","\t",$0);
       gsub("\n","",$0);
       ident =split($1,a," ")       # N.B the seq idents (supr all text after the first space)
       Seqlength = length($2) ;
       print a[1]"\t"Seqlength > refseqs
      }' $out/$ref".fasta_mod"
    done
    
    # regroup all samples
    refseqALL=$out/ALL_transcripts.ctgs
    cat $out/*.ctgs > $refseqALL
     
    # mapping successively every read sets on all transcripts.
    > $INDIR/jobs/rapmap-quasimap.cmds
    > $INDIR/jobs/rapmap-awk.cmds
    for (( k=0; k <i; k++ ))
    do
       reads=${fasta_array[$k]}"_reads"
       fileout=$out/${fasta_array[$k]}"_vs_ALL.out"
       finalout=$out/"ALL_transcripts.all"
       echo -e "\nBuiling mapping ${fasta_array[$k]} reads"
    
         # calculate expression level with selected tool
            if [ $MODE == "u" ]; then
                    fastq=" -r "$INDIR"/"${fasta_array[$k]}".fastq"
                  else
                    fastq=" -1 "$INDIR"/"${fasta_array[$k]}".L.fastq -2 "$INDIR"/"${fasta_array[$k]}".R.fastq"
            fi
            echo "rapmap quasimap -t $3 -i $out/$toolidx $fastq awk -v reads=$reads -v refseqs=$refseqALL $fileout" >> $INDIR/jobs/rapmap-quasimap.cmds
    done
fi

# extract split fasta to build split indexes
if [ $2 == "splitfasta" ]; then
    # getting length of transcripts
    for (( j=0; j <i; j++ ))
    do
      ref=${fasta_array[$j]};
      refseqs=$out/$ref".ctgs"
      echo -e "Getting length of $ref transcripts"
      awk -v ref=$ref -v refseqs=$refseqs 'BEGIN{RS=">"; FS="\t"}
      NR>1   {
       sub("\n","\t",$0);
       gsub("\n","",$0);
       ident =split($1,a," ")       # N.B the seq idents (supr all text after the first space)
       Seqlength = length($2) ;
       print a[1]"\t"Seqlength > refseqs
      }' $out/$ref".fasta_mod"
    done

    # regroup all samples
    refseqALL=$out/ALL_transcripts.ctgs
    cat $out/*.ctgs > $refseqALL
    echo "Done computing transcritps length in $refseqALL"

    source "$my_dir/splitfasta.sh"
    exit 1
fi

# build split indexes
if [ $2 == "splitindex" ]; then
    source "$my_dir/splitindex.sh"
    exit 1
fi

# map read and consolidate individual mapping reports
if [ $2 == "splitmap" ]; then
    source "$my_dir/splitmap.sh"
    source "$my_dir/consolidate.sh"
    exit 1
fi

