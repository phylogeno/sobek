### initial version of the section :
# "all pairwise BLAST and listing suspects (see $SUSPID and $SUSPLEN)"
> $INDIR/jobs/blastn.cmds
for (( j=0; j <i; j++ )); do
 ref=${fasta_array[$j]};
 suspects=$out/$ref".suspects"
 for (( k=0; k <i; k++ )); do
  target=${fasta_array[$k]};
  if [ "$ref" != "$target" ]; then
   outblast=$out/$ref"v"$target".outblast"
   query=$out/$ref".fasta_mod"
   db=$out/$target".blastdb"
   echo -e "Setting up BLAST: $ref vs. $target"

   echo "blastn -num_threads $PROCESSORS -query $query -db $db -perc_identity $SUSPID -soft_masking true -max_target_seqs 5000 -out $outblast" >> $INDIR/jobs/blastn.cmds
  fi
 done
done

for LIST in $INDIR/jobs/blastn.cmds; do tpage  --define T=`wc -l $LIST | cut -f1 -d" "`  --define list=$LIST $my_dir/templates/jobarray-croblastn.tt > $LIST-array.job; done
