> $INDIR/jobs/consolidate.cmds
for (( j=0; j <i; j++ )); do echo "$my_dir/consolidate.pl --indir $out/split-index/ --ctgfile=$out/ALL_transcripts.ctgs --basename=${fasta_array[$j]} --suffix=.out"; done >> $INDIR/jobs/consolidate.cmds

# 
for LIST in $INDIR/jobs/consolidate.cmds; do tpage --define T=$(wc -l $LIST | cut -f1  -d" ")  --define list=$LIST --define mail=1  $my_dir/templates/jobarray-rapmap-splitindex.tt > $LIST-array.job; done
