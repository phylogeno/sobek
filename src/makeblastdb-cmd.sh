# add sample name to all sequence names and build BLASTdb
if [ ! -d $INDIR/jobs ]; then
    mkdir $INDIR/jobs
fi
> $INDIR/jobs/makeblastdb.cmds 
for (( j=0; j <i; j++ )); do
	ref=${fasta_array[$j]}
	awk '/^>/{ print $1 } ; /^[^>]/{ print $0 }' < $INDIR/${fasta_array[$j]}.fasta > $out/${fasta_array[$j]}".fasta_mod"
	sed -i "s/>/>$ref|/g" $out/${fasta_array[$j]}".fasta_mod"
	echo "makeblastdb -in $out/${fasta_array[$j]}.fasta_mod -parse_seqids -dbtype nucl -out $out/${fasta_array[$j]}.blastdb" >> $INDIR/jobs/makeblastdb.cmds 
done

for LIST in $INDIR/jobs/makeblastdb.cmds; do tpage --define T=`wc -l $LIST | cut -f1 -d" "`  --define list=$LIST $my_dir/templates/jobarray-simple.tt > $LIST-array.job; done
