# setup split quasimap commands 
> $INDIR/jobs/split-quasimap.cmds
for IDX in $( cat $out/split-index/indexes.idl ); do
    for (( j=0; j <i; j++ )); do
        tpage --define indir=$indir --define outdir=$out --define cpus=20 --define basename=${fasta_array[$j]} --define index=$IDX $my_dir/templates/rapmap-splitindex.tt; 
    done; 
done >> $INDIR/jobs/split-quasimap.cmds

# setup split quasimap jobarray
for LIST in $INDIR/jobs/split-quasimap.cmds; do tpage --define T=$(wc -l $LIST | cut -f1  -d" ")  --define list=$LIST --define mail=1  $my_dir/templates/jobarray-rapmap-splitindex.tt > $LIST-array.job; done

