### OUTPUT : fasta files sorted by categories (clean, contam, dubious, lowcov, overexp)
START_TIME=$SECONDS
if [ $OUTPUTLEVEL == "1" ]; then
	echo -e "\nfasta files will not be written (output level set to '1')"

elif [ $OUTPUTLEVEL == "2" ]; then
	echo -e "\nWriting categorized transcriptomes - all categories (it might take some time)"

	for fasta in $out/*.fasta_mod; do
		ref=`basename $fasta .fasta_mod`
		echo -e "\t$ref" 

        $my_dir/output_fasta.pl $fasta $out/$ref.all

		if [ -f $out/$ref"_clean.fasta" ]; then sed -i "s/$ref|//g" $out/$ref"_clean.fasta"; fi
		if [ -f $out/$ref"_lowcov.fasta" ]; then sed -i "s/$ref|//g" $out/$ref"_lowcov.fasta"; fi
    done

    ### intermediate timing
    ELAPSED_TIME=$(($SECONDS - $START_TIME))
    h=$(($ELAPSED_TIME/3600))
    m=$((($ELAPSED_TIME%3600)/60))
    s=$(($ELAPSED_TIME%60))
    echo -e "\nExecution time with a perl-powered solution : $h h $m m $s s"

else echo -e "\nwarning : output level value must be set to either '1' or '2' (default = '2')"
fi

