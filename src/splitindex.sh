# setup quasiindex commands
for f in $out/split-index/ALL_transcripts.shuf-a?.fasta; do echo "rapmap quasiindex -t $f -p -x 20 -i ${f}_index"; done > $INDIR/jobs/split-quasiindex.cmds

# setup quasiindex jobarray
for LIST in $INDIR/jobs/split-quasiindex.cmds; do tpage --define T=`wc -l $LIST | cut -f1 -d" "` --define list=$LIST  $my_dir/templates/jobarray-simple.tt > $LIST-array.job; done
