#!/bin/bash
# This tool will analyse all the .fasta files in the current directory
# The reads files must be in the same dir and must follow this naming convention
# FOR paired end: NAME.fasta,  NAME.L.fastq,  NAME.R.fastq
# FOR unpaired  : NAME.fasta,  NAME.fastq
my_dir="$(dirname "$0")"

source "$my_dir/path_management.sh"
source "$my_dir/MacLin_management.sh"

############################ DEFAULT VALUES ############################
source "$my_dir/def_values.sh"

############################ CROCO USAGE ############################
source "$my_dir/parse_params.sh"

# summary of settings
source "$my_dir/print_settings.sh"

# setting output directory
if [ $RECAT == "no" ]; then
 out="$INDIR/${OUTPUTPREFIX}CroCo-"$TOOL"-id"$SUSPID"-len"$SUSPLEN"-fold"$FOLD"-mincov"$MINCOV"-"`date +"%Y_%m_%d-%I_%M"`
 echo "Output directory : $out"
 echo
 mkdir $out
elif [ $RECAT != "yes" ]; then
 out=$INDIR/${OUTPUTPREFIX}CroCo"_RECAT-fold"$FOLD"-mincov"$MINCOV"-"`date +"%Y_%m_%d-%I_%M"`
 echo "Output directory : $out"
 echo
 mkdir $out
fi

############################ PROGRAM BEGINS HERE ############################
START_TIME=$SECONDS

## run BLAST, mapping, and quantification if recat function is off (default)
if [ $RECAT == "no" ]; then

## setting sample name#s
 i=0
 for fasta in $INDIR/*.fasta ; do
   fasta_array[$i]=`basename $fasta .fasta`
   i=$(( i + 1 ))
 done

 echo -e "\n"
 source "$my_dir/makeblastdb-cmd.sh"
 echo "Done writing makeblastdb cmd file: $INDIR/jobs/makeblastdb.cmds" 
 echo -e "\n"
 source "$my_dir/blastn-cmd.sh"
 echo "Done writing blastn cmd file: $INDIR/jobs/blastn.cmds"
fi

### store variables
> $INDIR/variables.sh
echo "FOLD=$FOLD" >> $INDIR/variables.sh
echo "INDIR=$INDIR" >> $INDIR/variables.sh
echo "out=$out" >> $INDIR/variables.sh
echo "MINCOV=$MINCOV" >> $INDIR/variables.sh
echo "RECAT=$RECAT" >> $INDIR/variables.sh
echo "MODE=$MODE" >> $INDIR/variables.sh
echo "PROCESSORS=$PROCESSORS" >> $INDIR/variables.sh
echo "ADDOPT=$ADDOPT" >> $INDIR/variables.sh
echo "TOOL=$TOOL" >> $INDIR/variables.sh
echo "OVEREXP=$OVEREXP" >> $INDIR/variables.sh
echo "OUTPUTLEVEL=$OUTPUTLEVEL" >> $INDIR/variables.sh
echo "GRAPH=$GRAPH" >> $INDIR/variables.sh
chmod a+x $INDIR/variables.sh

#### store variables
#> $INDIR/variables.sh
#echo "FOLD=$FOLD" >> $INDIR/variables.sh
#echo "INDIR=$INDIR" >> $INDIR/variables.sh
#echo "out=$out" >> $INDIR/variables.sh
#echo "MINCOV=$MINCOV" >> $INDIR/variables.sh
#echo "RECAT=$RECAT" >> $INDIR/variables.sh
#echo "MODE=$MODE" >> $INDIR/variables.sh
#echo "PROCESSORS=$PROCESSORS" >> $INDIR/variables.sh
#echo "ADDOPT=$ADDOPT" >> $INDIR/variables.sh
#echo "TOOL=$TOOL" >> $INDIR/variables.sh
#echo "OVEREXP=$OVEREXP" >> $INDIR/variables.sh
#echo "OUTPUTLEVEL=$OUTPUTLEVEL" >> $INDIR/variables.sh
#echo "GRAPH=$GRAPH" >> $INDIR/variables.sh
#chmod a+x $INDIR/variables.sh
