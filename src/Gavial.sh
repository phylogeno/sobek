my_dir="$(dirname "$0")"
source "$1"
START_TIME=$SECONDS

## mapping successively every read sets on all transcripts.

source "$my_dir/mapping-read-sets.sh"

#perl -F"\t" -anle 'if (m/^Contig/) { push @species, $F[1] } else { $stat_for{$F[0]}{$species[-1]} = $F[1] }; END{ print join "\t", "Contig", @species; for $contig (keys %stat_for) { print join "\t", $contig, join "\t", map { $stat_for{$contig}{$_} || 0 } @species  } }' $out/*_vs_ALL.out > "$out/ALL_transcripts.all"

#cp "$out/ALL_transcripts.all" "$out/ALL_transcripts.quants"

#for (( k=0; k <i; k++ ))
#do
  #reads=${fasta_array[$k]}"_reads"
  #fileout=$out/${fasta_array[$k]}"_vs_ALL.out"
  #finalout=$out/"ALL_transcripts.all"
  #echo -e "\nMapping ${fasta_array[$k]} reads"

  #if [ -f $finalout ]
  #then
    ##join -t $'\t'  --header $finalout $fileout > $finalout'.tmp'
    #paste $finalout $fileout | awk -F 'FS' 'BEGIN{FS="\t"}{for (i=1; i<=NF-1; i++) if(i!=NF-1) {printf $i FS};{print $NF}}' > $finalout'.tmp'
    #mv $finalout'.tmp' $finalout
  #else
    #cat $fileout > $finalout
    #cp $finalout $out/ALL_transcripts.quants
  #fi
#done

### setting sample names
i=0
    for fasta in $INDIR/*.fasta ; do
    fasta_array[$i]=`basename $fasta .fasta`
    i=$(( i + 1 ))
    echo -e "\nfasta array $i: ${fasta_array[$k]}"
done

 # analyse mapping/count results: categorizing transcipts (clean, contam, dubious, lowcov, overexp)
 # care for character ";" in sequence names  ?
 # splitting "All_transcript.all" file into files corresponding to samples
 #cp $out/ALL_transcripts.quants $out/ALL_transcripts.all
 finalout=$out/ALL_transcripts.all
 for (( j=0; j <i; j++ ))
 do
   ref=${fasta_array[$j]}
   echo -e "\nCategorization of $ref transcripts"
   echo -e `head -n1 $finalout` > $out/$ref".all"
   grep "$ref|" $out/ALL_transcripts.quants >> $out/$ref".all"

   #readarray -t suspects < $out/$ref".suspects"
   #listeSuspects=$( IFS=';'; echo "${suspects[*]}" );
   awk -v outDir=$out -v ref=$ref -v col=$j -v fold=$FOLD -v mincov=$MINCOV -v overexp=$OVEREXP 'BEGIN{OFS="\t";col=col+2;}
   { if (NR == 1) {
        print $0, "MaxOtherSpCov", "log2FoldChange", "Status" > outDir"/"ref".tmp"
	  }
      else {
        refCount = $col; ok="clean"; maxcov=0; nb_overexp=0;
        for (i=2; i<= NF; i++) {
		  if (i != col) {
		     if ($i >= overexp) nb_overexp++
			 if ($i >= ( refCount * fold) && ($i >= mincov ) ) ok="contam"
			 else {
			   if ($i >= ( refCount / fold) && ($i >= mincov ) && ( ok != "contam" ) ) ok="dubious"
			 }
			 if ( $i > maxcov ) maxcov= $i
          }
        }
	   	if (maxcov < mincov && refCount < mincov ) ok="lowcov";
	    if (refCount == 0) refCount=0.0001
		if (nb_overexp >= 3) ok="overexp";
	    print $0, maxcov, log(refCount)/log(2) - log(maxcov)/log(2), ok > outDir"/"ref".tmp"
	    print $0 > outDir"/"ref"."ok
      }
   }' $out/$ref".all"
   mv $out"/"$ref".tmp" $out/$ref".all"

   # filtering out unsuspected transcripts
   echo -e `head -n1 $finalout`"\tMaxOtherSpCov\tlog2FoldChange\tStatus" > $out/$ref".all_suspectonly"
   nb=`cat $out/$ref".suspects" | wc -l`
   LC_ALL=C grep -F -w -m$nb -f $out/$ref.suspects $out/$ref.all >> $out/$ref".all_suspectonly" 
   mv $out/$ref".all" $out/$ref".all_quants" ; mv $out/$ref".all_suspectonly" $out/$ref".all"
   sed -i 's/ /\t/g' $out/$ref".all"
   # moving unnecessary files
 done


### OUTPUT : basic statistics (cross_contamination_summary and cross_contamination_profiles files)
source "$my_dir/output_stats.sh"

### OUTPUT : fasta files sorted by categories (clean, contam, dubious, lowcov, overexp)
OUTPUTLEVEL=2
#source "$my_dir/output_fasta.sh"
source "$my_dir/output_fasta_pl.sh"

### OUTPUT : html file (detailed results for all transcripts)
#source "$my_dir/output_html.sh"

### OUTPUT : network files - step 1 ("LINKS.csv_exhaustive" and "nodes_detailed.csv")
source "$my_dir/output_network_basefiles_contam.sh"

### OUTPUT : network files - step 2 ("LINKS_exhaustive_dubious.csv" and "nodes_detailed_dubious.csv" -- using only dubious)
source "$my_dir/output_network_basefiles_dubious.sh"

### OUTPUT : network files - step 3 ("LINKS.csv")
#echo -e "\tLINKS_gephi.csv\n\tLINKS_diagrammer.tsv\n\tLINKS_gephi_dubious.csv\n\tLINKS_diagrammer_dubious.tsv"
source "$my_dir/output_network_LINKS_files.sh"

### OUTPUT : network files - step 4 ("LINKS_gephi_simplified.csv", "LINKS_diagrammer_simplified.tsv")
#echo -e "\tLINKS_gephi_simplified.csv\n\tLINKS_diagrammer_simplified.tsv"
#echo -e "\tLINKS_gephi_dubious_simplified.csv\n\tLINKS_diagrammer_dubious_simplified.tsv"
source "$my_dir/output_network_LINKS_simplified_files.sh"

### OUTPUT : network files - step 5 (various 'nodes' files)
#echo -e "\tNODES_gephi_absolute.csv\n\tNODES_gephi_norm.csv\n\tNODES_diagrammer.tsv\n\tNODES_gephi_dubious_absolute.csv\n\tNODES_gephi_dubious_norm.csv\n\tNODES_diagrammer_dubious.tsv"
source "$my_dir/output_network_NODES_files.sh"

### miscellaneous
mkdir network_info
mv LINKS_* NODES_* nodes_detailed*.csv network_info

### GRAPHICAL OUTPUT : network visualization ("network.html")
if [ $GRAPH == "no" ]; then
    echo -e "\nNo graphical output will be written (graph parameter value set to 'no')"
elif [ $GRAPH == "yes" ] ; then
    echo -e "\nRendering graphical visualizations of cross contamination network\n\tnetwork_complete.html\n\tnetwork_simplified.html\n\tnetwork_dubious_complete.html\n\tnetwork_dubious_simplified.html"
    R --quiet --vanilla < $crosscontamdir/visNetwork.R 2>&1 >/dev/null
fi

### miscellaneous
if [ $RECAT == "no" ]; then
    mkdir utility_files_CroCo
    mv *.outblast *.suspects *.blastdb.* *_index *.ctgs *.out *.all_quants *.fasta_mod *.fasta_suspect utility_files_CroCo/
fi
# moving unnecessary files
rm -f $out/ALL_transcripts.fasta_suspects 
cd ../

### final timing
ELAPSED_TIME=$(($SECONDS - $START_TIME))
h=$(($ELAPSED_TIME/3600))
m=$((($ELAPSED_TIME%3600)/60))
s=$(($ELAPSED_TIME%60))
echo -e "\nAll results are in $out\n\nCroCo run finished (total run time : $h h $m m $s s)"
