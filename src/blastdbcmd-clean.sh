### initial version of the section :
# "all pairwise BLAST and listing suspects (see $SUSPID and $SUSPLEN)"
 echo -e "\n"
 for (( j=0; j <i; j++ )); do
  ref=${fasta_array[$j]};
  suspects=$out/$ref".suspects"
  cat $out/$ref".fasta_suspect_tmp" | sed 's/lcl|//g' | awk '{if($0 ~ /^>/){print $1} else{print}}' > $out/$ref".fasta_suspect"
  rm -f $out/$ref".fasta_suspect_tmp"
  echo -e "\ttotal suspects transcripts in $ref : "`cat $out/$ref".suspects" | wc -l`
 done

 # regroup all samples
 cat $out/*.fasta_mod > $out/ALL_transcripts.fasta
 cat $out/*.suspects > $out/ALL_transcripts.suspects
 cat $out/*.fasta_suspect > $out/ALL_transcripts.fasta_suspects
