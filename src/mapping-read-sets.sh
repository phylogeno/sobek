source "$1"
START_TIME=$SECONDS
echo "Mapping successively every read sets on all transcripts..."

perl -F"\t" -anle 'if (m/^Contig/) { push @species, $F[1] } else { $stat_for{$F[0]}{$species[-1]} = $F[1] }; END{ print join "\t", "Contig", @species; for $contig (keys %stat_for) { print join "\t", $contig, join "\t", map { $stat_for{$contig}{$_} || 0 } @species  } }' $out/*_vs_ALL.out > "$out/ALL_transcripts.all"
 
cp "$out/ALL_transcripts.all" "$out/ALL_transcripts.quants"

### final timing
ELAPSED_TIME=$(($SECONDS - $START_TIME))
h=$(($ELAPSED_TIME/3600))
m=$((($ELAPSED_TIME%3600)/60))
s=$(($ELAPSED_TIME%60))
