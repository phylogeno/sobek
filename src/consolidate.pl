#!/usr/bin/env perl 

use autodie;
use Modern::Perl '2011';

use Smart::Comments;

use File::Find::Rule;
use Getopt::Euclid qw(:vars);

# read contig file and fetch contig size
my %size_for;
open my $in, '<', $ARGV_ctgfile;

while (my $line = <$in>) {
    chomp $line;
    my ($ctg, $size) = split /\t/xms, $line;
    $size_for{$ctg} = $size;
} 
### Done reading ctgs file

my @infiles = File::Find::Rule
    ->file()
    ->name( qr{ ^$ARGV_basename .* $ARGV_suffix$ }xmsi )
    ->maxdepth(1)
    ->in($ARGV_indir)
;
### @infiles

my %count_for;
for my $infile (@infiles) {
    open my $table, '<', $infile;
    while (my $line = <$table>) {
        next if $line =~ m/^Contig/xmsg; 
        next if $line =~ m/^$/xmsg;
        chomp $line;
        my ($ctg, $count) = (split /\t/xms, $line);
        $count_for{$ctg} += $count || 0;
    }
}
### Done counting mapped contig stats

my $totRPK;
map { $totRPK += $count_for{$_}/$size_for{$_} } keys %count_for;

my $outdir  = dirname($ARGV_ctgfile);
### $outdir
my $outfile = $outdir . "/" . $ARGV_basename . '_vs_ALL.out';
### $outfile
open my $out, '>', $outfile;

# write headers
say {$out} join "\t", 'Contig', $ARGV_basename . '_reads'; 

if ($totRPK > 0) {
    my $norm=(1/$totRPK)*1000000;
    ### $norm

    for my $ctg ( keys %count_for ) { 
        my $val = ($count_for{$ctg}/$size_for{$ctg})*$norm;
        say {$out} join "\t", $ctg, $val; 
    } 
}
else { 
    say {$out} join "\t", $_, '0' for keys %count_for;
} 
### Done combinating tables and computing stats



=head1 NAME

consolidate.pl

=head1 VERSION


=head1 USAGE


=head1 REQUIRED ARGUMENTS

=over

=item --indir <dir>

Path to input files directory.

=for Euclid: 
    dir.type: str

=item --basename=<str>

Basnemae for current organism

=for Euclid:
    str.type: string

=item --suffix=<str>

Suffix for input file(s)

=for Euclid:
    str.type: string

=item --ctgfile=<file>

Contig size file

=for Euclid:
    file.type: readable

=back

=head1 OPTIONAL ARGUMENTS

=over

=back
