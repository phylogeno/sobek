### initial version of the section :
# "all pairwise BLAST and listing suspects (see $SUSPID and $SUSPLEN)"
echo -e "\n"
> $INDIR/jobs/blastdbcmd.cmds
for (( j=0; j <i; j++ )); do
  ref=${fasta_array[$j]};
  echo $ref
  suspects=$out/$ref".suspects"
  echo $suspects
  cat $out/$ref"v"*".outblast" > $out/$ref".outblast" ; rm -f $out/$ref"v"*".outblast"
  cat $out/$ref".outblast" | awk -v susplen=$SUSPLEN '{ if($5>=susplen){print $1} }' > $out/$ref".suspects_tmp"
  cat $out/$ref".suspects_tmp" | sort | uniq > $out/$ref".suspects" ; rm -f $suspects"_tmp"
  echo "blastdbcmd -db $out/$ref.blastdb -entry_batch $out/$ref.suspects -outfmt %f -line_length 20000 -out $out/$ref.fasta_suspect_tmp" >> $INDIR/jobs/blastdbcmd.cmds
done

echo "Done preparing blastdbcmd commands..."

for LIST in $INDIR/jobs/blastdbcmd.cmds; do tpage --define T=$(wc -l $LIST | cut -f1 -d" ")  --define list=$LIST $my_dir/templates/jobarray-simple.tt > $LIST-array.job; done

echo "Done preparing blastdbcmd job-array..."
