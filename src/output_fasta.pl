#!/usr/bin/env perl
# PODNAME: yaml-generator-42.pl
# ABSTRACT: Interactive or batch generator for 42 YAML config files
# CONTRIBUTOR: Mick VAN VLIERBERGHE <mvanvlierberghe@doct.uliege.be>

use autodie;
use Modern::Perl '2011';

use Smart::Comments;

use Bio::MUST::Core;
use Bio::MUST::Core::Utils qw(change_suffix);
use aliased 'Bio::MUST::Core::Ali::Stash';
use aliased 'Bio::MUST::Core::IdList';

my $database = shift;
### $database
my @infiles  = @ARGV;
### @infiles

for my $infile (@infiles) {
    my %seq_id_for;
    my $org = $infile;
       $org =~ s/\.all//xms;
    
    # get lists
    open my $in, '<', $infile;
    while (my $line = <$in> ) {
        chomp $line;
        next if $line =~ m/^Contig/xms;
        next if $line =~ m/clean$/xms;
        my ($seq_id, $category) = (split /\t/xms, $line)[0,-1];
        push @{ $seq_id_for{$category} }, $seq_id;
    }
    
    # load database
    my $db = Stash->load($database);

    my @not_clean_ids;
    for my $category ( keys %seq_id_for ) {

        ### Processing: $category
        my @ids  = @{ $seq_id_for{$category} };
        push @not_clean_ids, @ids;
        my $list = IdList->new( ids => \@ids );

        # assemble Ali and store it as FASTA file
        my $ali = $list->filtered_ali($db);
           $ali->dont_guess;
        my $outfile = $org . "_$category.fasta";
           $ali->store_fasta($outfile);
    }

    my $list = IdList->new( ids => \@not_clean_ids );
#       $list = $list->negative_list($ali);
       $list = $list->negative_list($db);
    
    # assemble Ali and store it as FASTA file
    my $ali = $list->filtered_ali($db);
       $ali->dont_guess;
    my $outfile = $org . "_clean.fasta";
       $ali->store_fasta($outfile);
}
